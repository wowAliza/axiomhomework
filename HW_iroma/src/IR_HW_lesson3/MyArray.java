package IR_HW_lesson3;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Iterator;

public class MyArray<T> implements Iterable<T> {

    private T[] array;

    MyArray(Class<T> clazz){
        try{
        this.array = (T[]) Array.newInstance(clazz, 2);
    } catch (Exception e) {
            System.out.println("Incorrect array type");
            e.getStackTrace();
        }
    }

    public int length(){
        return array.length;
    }

    public T getItem(int index){
        if (index >= array.length || index < 0){
            System.out.println("Incorrect array index");
            return null;
        } else {
            return this.array[index];
        }
    }


    @Override
    public Iterator iterator(){
        return new ArrayIterator(this);
    }

    void add(T str) {
        int latestIndex = size();
        if (latestIndex < array.length) {
            this.array[latestIndex] = str;
        } else {
            T[] newArr = Arrays.copyOf(this.array, array.length * 2);
            newArr[latestIndex] = str;
            this.array = newArr;
        }
    }

    public int size() {
        int size = 0;
        for (T x : array) {
            if (x != null) {
                size++;
            }
        }
        return size;
    }

        void remove(int index){
            if (index >= array.length || index < 0){
                System.out.println("Incorrect array index");
            } if (size() == 0) {
                System.out.println("The array is empty");
            } else {
                int i = index;
                while (i < array.length -1) {
                    this.array[i] = this.array[i+1];
                    i++;
                }
                this.array[array.length - 1] = null;
            }
        }

        void remove(T item){
            if (item == null){
                System.out.println("Incorrect entry");
            } if (size() == 0) {
                System.out.println("The array is empty");
            } else {
                T[] newStr = (T[]) new Object[array.length];
                int i = 0;
                for (T s : array) {
                    if (!(s == item)) {
                        newStr[i] = s;
                        i++;
                    }
                }
                array = newStr;
            }
        }

        void printArr(){
            System.out.println(Arrays.toString(this.array));
        }


    }

