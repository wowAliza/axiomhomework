package IR_HW_lesson3;

import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayIterator<T> implements Iterator {
    private int index = 0;
    private T item;
    private MyArray<T> items;


    public ArrayIterator(MyArray<T> items) {
         this.items = items;
    }

    @Override
    public boolean hasNext() {
        return index < items.length() - 1;
    }

    @Override
    public T next() throws NoSuchElementException {
        try {
            T result = items.getItem(index);
            if (index < items.length() - 1) {
                index++;
            }
            return result;
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }



}
