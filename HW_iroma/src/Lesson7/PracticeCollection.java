package Lesson7;

import java.util.*;

public class PracticeCollection {
    public static void main(String[] args) {

        Collection col = createCollection();

        for (Object o : col){
            System.out.println("Item: " + o);
        }

        System.out.println("---------------------");

        for (Iterator it = col.iterator(); it.hasNext(); ){
            String s = (String)it.next();
            System.out.println("Item: " + s);
        }

        System.out.println("---------------------");

        Collection col2 = new ArrayList();
        col2.add("1");
        col2.add("2");
        col2.add("3");

        if(col.containsAll(col2)){
            System.out.println("TRUE");
        }

        System.out.println("---------------------");

        col.addAll(col2);
        for(Object o : col){
            System.out.println("Item: " + o);
        }

        System.out.println("---------------------");

        col.removeAll(col2);
        for(Object o : col){
            System.out.println("Item: " + o);
        }

        System.out.println("---------------------");

        col = createCollection();
        col.retainAll(col2);
        for(Object o : col){
            System.out.println("Item: " + o);
        }

        System.out.println("---------------------");

        col.clear();
        for(Object o : col){
            System.out.println("Item: " + o);
        }

        System.out.println("---------------------");

        col = createCollection();
        col.remove("2");
        for(Object o : col){
            System.out.println("Item: " + o);
        }

        System.out.println("---------------------");

        while (!col.isEmpty()){
            Iterator it = col.iterator();
            Object o = it.next();
            System.out.println("Remove: " + o);
            it.remove();
        }


    }

    private static Collection createCollection() {
        Collection c = new ArrayList();
        c.add("1");
        c.add("2");
        c.add("3");
        c.add("4");
        c.add("5");
        return c;
    }
}
