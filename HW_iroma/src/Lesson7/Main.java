package Lesson7;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class Main {
    public static void main(String[] args) throws IllegalAccessException, InstantiationException {

        Map<String, String> map = new LinkedHashMap<>();
        map.put("One", "Fisrt");
        map.put("Two", "Second");
        map.put("Three", "Third");
        map.put("Four", "Four");

    }
    public void printMap(Map<String, String> map){
        for (Map.Entry<String, String> entry: map.entrySet()){
            System.out.println("Key = " + entry.getKey() + "  Value = " + entry.getValue());
        }

//        System.out.println("Choose figure name: ");
//        System.out.println(Figures.SQUARE.name());
//        System.out.println(Figures.TRIANGLE.name());
//        System.out.println(Figures.CIRCLE.name());
//        System.out.println(Figures.ELLIPSE.name());
//        Scanner sc = new Scanner(System.in);
//        String text = sc.nextLine();
//        System.out.println(Figures.valueOf(text));
        ArrayList<String> s = new ArrayList<>();
        MyClass cl = new MyClass();
        try {
            Method method = cl.getClass().getDeclaredMethod("myMethod"); // кладем метод в объект метод
            method.setAccessible(true); // делаем метод доступныс
            method.invoke(cl); //вызываем метод

        }  catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }

        Class<A> clazz = A.class;
        //A instance = clazz.newInstance();

        A instance2 = new A("Joe", 22);
    }
}
