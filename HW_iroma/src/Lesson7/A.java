package Lesson7;

public class A {
    String name;
    int age;

    A(String n, int a){
        this.name = n;
        this.age = a;
    }

    @Override
    public String toString(){
        return String.format("{name = %s, age = %d}", name, age);

    }


}
