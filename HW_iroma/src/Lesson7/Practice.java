package Lesson7;

import IR_HW_lesson4.Circle;

import java.util.*;

public class Practice {
    public static void main(String[] args) {

        System.out.println("FIRST");
        List<Integer> l = new ArrayList<Integer>(Arrays.asList(89, 99, 102, 5, 68, 91, 28));
        sortList(l);
        System.out.println("---------------------------------");

        System.out.println("SECOND");
        List<A> person = Arrays.asList(
                new A("Dora", 66),
                new A("Aurora", 24),
                new A("Miriam", 31),
                new A("Marat", 12),
                new A("Norman", 85)
            );
        Collections.sort(person, new ComparatorAge());
        System.out.println(person);
        System.out.println("---------------------------------");

        System.out.println("THIRD");
        Set<Integer> mySet1= new TreeSet<Integer>(Arrays.asList(10, 100, 89, 65, 2, 33, 15));
        System.out.println(Arrays.toString(mySet1.toArray()));
        Set<Integer> mySet2= new HashSet<Integer>(Arrays.asList(10, 100, 89, 65, 2, 33, 15));
        System.out.println(Arrays.toString(mySet2.toArray()));


    }

    static void sortList(List<Integer> list){
        int x = 0;
        for(int i = list.size() - 1; i > 0; i--){
            for (int j = 0; j < i; j++){
                if (list.get(j) > list.get(j + 1)){
                    x = list.get(j);
                    list.set(j, list.get(j + 1));
                    list.set(j + 1, x);
                 }
            }
        }
        System.out.println(Arrays.toString(list.toArray()));
    }
}
