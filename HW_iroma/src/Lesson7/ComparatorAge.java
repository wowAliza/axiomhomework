package Lesson7;

import java.util.Comparator;

public class ComparatorAge  implements Comparator<A> {
    @Override
    public int compare(A a, A b){
        return a.age < b.age ? -1 : a.age == b.age ? 0 : 1;

    }
}
