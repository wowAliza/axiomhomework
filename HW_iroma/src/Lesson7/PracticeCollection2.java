package Lesson7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class PracticeCollection2 {
    public static void main(String[] args) {

        ArrayList<String> str = new ArrayList<>();
        str.add("STOP");
        str.add("SHOP");
        str.add("MOVE");
        str.add("NONE");
        str.add("CLONE");
        str.add("DOOR");

        Collections.shuffle(str);
        System.out.println(Arrays.toString(str.toArray()));

        Collections.reverse(str);
        System.out.println(Arrays.toString(str.toArray()));

        Collections.rotate(str,2);
        System.out.println(Arrays.toString(str.toArray()));

        Collections.swap(str,2,3);
        System.out.println(Arrays.toString(str.toArray()));

        Collections.replaceAll(str,"CLONE","BOAL");
        System.out.println(Arrays.toString(str.toArray()));

        ArrayList<String> str2 = new ArrayList<>(str.size());
        for(String s : str){
            str2.add(null);
        }
        Collections.copy(str2,str);
        System.out.println(Arrays.toString(str.toArray()));
        System.out.println(Arrays.toString(str2.toArray()));


    }
}
