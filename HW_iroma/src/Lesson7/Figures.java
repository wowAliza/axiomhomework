package Lesson7;

import IR_HW_lesson4.Figure;

public enum Figures {
    SQUARE("Квадрат"),
    TRIANGLE("Треугольник"),
    CIRCLE("Окружность"),
    ELLIPSE( "Эллипс");

    String rusName;

    Figures(String rusName){
        this.rusName = rusName;
    }

    public String toString(){
        return rusName;
    }


}
