package Lesson7;

import java.util.*;
import java.util.stream.Collectors;

public class MyStream {
    public static void main(String[] args) {

        List<Integer> arr = Arrays.asList(10,15,24,88,94,36,50);

        List<Integer> num = arr.stream()
                .filter(i -> (i < 50) && (i > 20))
                .collect(Collectors.toList());

        num.forEach(System.out::println);

        List<A> myL = Arrays.asList(
                new A("DOOR", 256),
                new A("WINDOW", 104),
                new A("FLOW", 316),
                new A("WALL", 96),
                new A("CONNER", 48)
        );

        String s = "CONNER";
        A a = myL.stream()
                .filter(i -> s.equals(i.name))
                .findAny()
                .orElse(null);

        System.out.println(a);


    }
}
