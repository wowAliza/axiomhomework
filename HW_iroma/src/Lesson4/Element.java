package Lesson4;

public class Element {
    private int x;
    private int y;
    private String cssClass;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        if (x >= 0) {
            this.x = x;
        }
        System.out.println("x should be more or equal 0");
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        if (y >= 0) {
            this.y = y;
        }
        System.out.println("y should be more or equal 0");
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        if (cssClass != null) {
            this.cssClass = cssClass;
        }
        System.out.println("cssClass shouldn't be empty");
    }


    public Element() {}

    public Element(int x, int y){
        setX(x);
        setY(y);
    }

    public Object select(){
        return new Object();
    }




}
