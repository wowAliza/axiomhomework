package IR_HW_lesson8;

import java.lang.reflect.Method;

public class MyClass {


    public void method1(){
        System.out.println("Method 1 was started and completed");
    }


    public void method2(){
        System.out.println("Method 2 was started and completed");
    }

    @MyAnnotation
    public void method3(){
        System.out.println("Method 3 was started and completed");
    }


}


