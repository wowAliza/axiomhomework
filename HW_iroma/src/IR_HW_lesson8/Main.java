package IR_HW_lesson8;

import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args){

        MyClass myClass = new MyClass();
        Method[] methods = myClass.getClass().getMethods();

        int pass = 0;
        int fail = 0;

        for (Method method : methods) {
            if (method.isAnnotationPresent(MyAnnotation.class)) {
                try {
                    method.invoke(myClass);
                    pass++;
                } catch (Exception e) {
                    fail++;
                }
            }
        }
    }
}