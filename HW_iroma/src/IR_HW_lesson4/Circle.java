package IR_HW_lesson4;
import java.util.Scanner;

public class Circle extends Figure {


    @Override
    protected float getAria() {
        this.aria = (float) (PI * this.side_1);
        System.out.println("\u001B[0m" + "\n" + "Circle Area is: " + aria);
        return aria;
    }

    @Override
    protected float getPerimeter() {
        this.perimeter = (float) (2 * PI * this.side_1);
        System.out.println("\u001B[0m" + "\n" + "Circle Perimeter is: " + perimeter);
        return perimeter;

    }

    @Override
    public void area(){
        Scanner sc = new Scanner(System.in);

        System.out.print("\u001B[33m" + "Enter the Circle radius: ");
        setSide_1(sc.nextFloat());

        getAria();
    }

    @Override
    public void perimeter(){
        Scanner sc = new Scanner(System.in);

        System.out.print("\u001B[33m" + "Enter the Circle radius: ");
        setSide_1(sc.nextFloat());

        getPerimeter();
    }
}

