package IR_HW_lesson4;
import java.util.Scanner;

public abstract class Figure {
    protected float side_1;
    protected float aria;
    protected float perimeter;
    public static final double PI = Math.PI;

    void setSide_1(float side_1) {
        if (side_1 < 0) {
           throw new IllegalArgumentException("Incorrect entry");
        }
        this.side_1 = side_1;
    }

    protected float getSide_1() {
        return this.side_1;
    }

    abstract float getAria() ;

    abstract float getPerimeter();

    abstract void area();

    abstract void perimeter();


}

