package IR_HW_lesson4;
import java.util.Scanner;

public class Triangle extends Figure {
    private float side_2;
    private float side_3;
    private float height;


    protected float getHeight() {
        return this.height;
    }

    protected float getSide_2() {
        return side_2;
    }

    protected float getSide_3() {
        return side_3;
    }

    @Override
    protected float getAria() {
        this.aria = (this.side_1 * this.height) / 2;
        System.out.println("\u001B[0m" + "\n" + "Triangle Area is: " + aria);
        return aria;
    }

    @Override
    protected float getPerimeter() {
        this.perimeter = this.side_1 + this.side_2 + this.side_3;
        System.out.println("\u001B[0m" + "\n" + "Triangle Perimeter is: " + perimeter);
        return perimeter;
    }

    private void setHeight(float height) {
        if (height < 0) {
            throw new IllegalArgumentException("Incorrect entry");
        }
        this.height = height;
    }

    private void setSide_2(float side_2) {
        if (side_2 < 0) {
            throw new IllegalArgumentException("Incorrect entry");
        }
        this.side_2 = side_2;
    }

    private void setSide_3(float side_3) {
        if (side_3 < 0) {
            throw new IllegalArgumentException("Incorrect entry");
        }
        this.side_3 = side_3;
    }

    @Override
    public void area(){
        Scanner sc = new Scanner(System.in);

        System.out.print("\u001B[33m" + "Enter the base of the triangle: ");
        setSide_1(sc.nextFloat());

        System.out.print("\u001B[33m" + "Enter the height of the triangle: ");
        setHeight(sc.nextFloat());

        getAria();
    }

    @Override
    public void perimeter(){
        Scanner sc = new Scanner(System.in);

        System.out.print("\u001B[33m" + "Enter the length of fist side: ");
        setSide_1(sc.nextFloat());

        System.out.print("\u001B[33m" + "Enter the length of second side: ");
        setSide_2(sc.nextFloat());

        System.out.print("\u001B[33m" + "Enter the length of third side: ");
        setSide_3(sc.nextFloat());

        getPerimeter();
    }


}
