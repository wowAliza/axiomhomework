package IR_HW_lesson4;

import java.util.Scanner;

public class Square extends Figure {


    @Override
    protected float getAria() {
        this.aria = this.side_1 * this.side_1;
        System.out.println("\u001B[0m" + "\n" + "Square Area is : " + aria);
        return aria;
    }

    @Override
    protected float getPerimeter() {
        this.perimeter = 4 * this.side_1;
        System.out.println("\u001B[0m" + "\n" + "Square Perimeter is: " + perimeter);
        return perimeter;
    }

    @Override
    public void area(){
        Scanner sc = new Scanner(System.in);

        System.out.print("\u001B[33m" + "Enter the length of the Square side: ");
        setSide_1(sc.nextFloat());

        getAria();
    }

    @Override
    public void perimeter(){
        Scanner sc = new Scanner(System.in);

        System.out.print("\u001B[33m" + "Enter the length of the Square side: ");
        setSide_1(sc.nextFloat());

        getPerimeter();
    }
}


