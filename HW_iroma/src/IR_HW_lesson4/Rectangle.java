package IR_HW_lesson4;
import java.util.Scanner;

public class Rectangle extends Figure {
    private float side_2;


   protected float getSide_2() {
        return side_2;
    }

   private void setSide_2(float side_2) {
        if (side_2 < 0) {
            throw new IllegalArgumentException("Incorrect entry");
        }
        this.side_2 = side_2;
    }

    @Override
    protected float getAria() {
        this.aria = this.side_1 * this.side_2;
        System.out.println("\u001B[0m" + "\n" + "Rectangle Area is: " + aria);
        return aria;
    }

    @Override
    protected float getPerimeter() {
        this.perimeter = 2 * (this.side_1 + this.side_2);
        System.out.println("\u001B[0m" + "\n" + "Rectangle Perimeter is: " + perimeter);
        return perimeter;

    }

    @Override
    public void area(){
        Scanner sc = new Scanner(System.in);

        System.out.print("\u001B[33m" + "Enter the length of the first Rectangle side: ");
        setSide_1(sc.nextFloat());

        System.out.print("\u001B[33m" + "Enter the length of the second Rectangle side: ");
        setSide_2(sc.nextFloat());

        getAria();
    }

    @Override
    public void perimeter(){
        Scanner sc = new Scanner(System.in);

        System.out.print("\u001B[33m" + "Enter the length of the first Rectangle side: ");
        setSide_1(sc.nextFloat());

        System.out.print("\u001B[33m" + "Enter the length of the second Rectangle side: ");
        setSide_2(sc.nextFloat());

        getPerimeter();
    }
}
