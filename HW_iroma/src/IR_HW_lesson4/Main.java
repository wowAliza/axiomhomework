package IR_HW_lesson4;
import java.util.Scanner;

public class Main {
    public static void main (String[] args){

        Figure figure = null;

        System.out.println("\u001B[34m" + "LET'S START A CALCULATION");
        System.out.println("\u001B[33m" + "Select one of figures:" + "\n" +
                "Square = 1  Triangle = 2  Rectangle = 3  Circle = 4  Ellipse = 5");

        Scanner sc = new Scanner(System.in);
        int selection = sc.nextInt();

        if (selection > 5 || selection < 1) {
           try {
               throw new IllegalArgumentException("Incorrect entry");
           } catch (IllegalArgumentException e){
               System.out.println("\u001B[31m" + "Incorrect selection" + "\n" + "PROGRAM THE END");
           }
        } else {

            switch (selection) {
                case 1:
                    figure = new Square();
                    System.out.println("\u001B[0m" + "Your choice is a Square");
                    break;
                case 2:
                    figure = new Triangle();
                    System.out.println("\u001B[0m" + "Your choice is a Triangle");
                    break;
                case 3:
                    figure = new Rectangle();
                    System.out.println("\u001B[0m" + "Your choice is a Rectangle");
                    break;
                case 4:
                    figure = new Circle();
                    System.out.println("\u001B[0m" + "Your choice is a Circle");
                    break;
                case 5:
                    figure = new Ellipse();
                    System.out.println("\u001B[0m" + "Your choice is a Ellipse");
                    break;
                default:

            }

            System.out.println("\u001B[33m" + "Select calculation type:" + "\n" + "Aria = A   Perimeter = P");
            String calc = sc.next();

            if (calc.matches("[aApP]")) {

                try {
                    switch (calc) {
                        case "A":
                        case "a":
                            figure.area();
                            break;
                        case "P":
                        case "p":
                            figure.perimeter();
                            break;
                    }
                } catch (IllegalArgumentException e){
                    System.out.println("\u001B[31m" + "Parameter should be more than zero" + "\n" + "PROGRAM THE END");
                }
            } else {
                System.out.println("\u001B[31m" + "Incorrect selection" + "\n" + "PROGRAM THE END");
            }
        }
    }

}
