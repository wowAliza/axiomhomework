package Lesson5;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args){
        finish(new Closable() {
            @Override
            public void close() {
                System.out.println("error");
            }
        });

    }



    public static void finish(Closable closableObject){
        closableObject.close();
     }
}
