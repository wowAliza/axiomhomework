package Lesson5;

public class Chrome extends Browser implements Closable{

    @Override
    public void close() {
        System.out.println("Closing Chrome");
    }
}
