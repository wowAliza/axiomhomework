package Lesson5;
import java.io.Closeable;

public class File implements Closable, Openable{

    @Override
    public void close() {
        System.out.println("Closing file");
    }

    @Override
    public void open() {

    }
}

