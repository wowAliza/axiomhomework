package Lesson5;

public interface Openable {
    int open = 1;
    int close = 0;

    void open();

    default void openClose(){
        System.out.println("Open and Close");
    }
}
