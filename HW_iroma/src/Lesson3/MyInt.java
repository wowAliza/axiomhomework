package Lesson3;

public class MyInt {
    int x;

    MyInt(int x){
        this.x = x;
    }

    void print(){
        if (Math.random() > 0.5){
            return; // as "breake"
        }
        System.out.println("print");
    }

    Student doSomething(int a, int z){
        if (a < 10){
            return new Student(z);
        }
        System.out.println("print");
        return new Student(a);
    }

    /**
     * This method deprecated, please, use other
     */
    @Deprecated
    Student doSomething(int a, String z){
        if (a < 10){
            return new Student(z);
        }
        System.out.println("print");
        return new Student(a);
    }
}
