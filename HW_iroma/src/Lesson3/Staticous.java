package Lesson3;

public class Staticous {

    int nonStaticField;
    static int staticField; // is own to the class

    public static void methodDo(){ // you can use only static variables
        staticField = 8;

    }

    class Test{

    }

    static class statTest{

    }

}
